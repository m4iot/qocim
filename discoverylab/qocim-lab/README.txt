QoCIM discovery lab.

This repository contains four folders:
 * qocim-lab-collector: the collector used to realise the exercice 1
 * qocim-lab-application: the application used to realise the exercice 1
 * qocim-lab-collector-exercise2: the collector used to realise the exercice 2
 * qocim-lab-application-exercise3: the applicationused to realise the exercice 3

WARNING: This framework depends on the muContext data model.

For more information, see the web page: https://fusionforge.int-evry.fr/www/qocim/lab/qocim-lab-exercices.html

To compile the discovery lab, use the following command: 
 $> mvn clean install -Djsse.enableSNIExtension=false
