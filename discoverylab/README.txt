The QoCIM discovery lab.

This repository contains two folders:
 * qocim-lab: a lab to discover the basic feature of QoCIM;
 * qocim-capsule-lab: a lab to discover more advanced feature of QoCIM..

WARNING: This framework depends on the muContext data model.

For more information, see the web page: https://fusionforge.int-evry.fr/www/qocim/

To compile the discovery lab, use the following command: 
 $> mvn clean install -Djsse.enableSNIExtension=false

