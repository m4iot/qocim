/**
 * This file is part of the QoCIM middleware.
 *
 * Copyright (C) 2014 IRIT, Télécom SudParis
 *
 * The QoCIM software is free software: you can redistribute it and/or modify
 * It under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The QoCIM software platform is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License 
 * for more details: http://www.gnu.org/licenses
 *
 * Initial developer(s): Pierrick MARIE
 * Contributor(s): 
 */
package qocim.cdfm.function.utils;

public class LogMessages {

    public final static String NEW_FUNCTION_INSTANCE = " - Creating a new instance.";

    public final static String ADD_CONTEXT_REPORT = " - Adding a new context report.";

    public final static String BEGIN_EXECUTION_FUNCTION = " - Begin the execution of the function.";

    public final static String END_EXECUTION_FUNCTION = " - End of the execution of the function.";

    public final static String SET_OPERATOR = " - Set operator: ";

    public final static String GET_OPERATOR = " - Get operator: ";

    public final static String SET_RESULT_LISTENER = " - Set result listener: ";

    public final static String GET_RESULT_LISTENER = " - Get result listener: ";

    public final static String SET_TIME_TO_WAIT = " - Set result listener: ";

    public final static String GET_TIME_TO_WAIT = " - Get result listener: ";

    public final static String SET_NB_HANDLED_MESSAGES = " - Set nb handled messages: ";

    public final static String GET_NB_HANDLED_MESSAGES = " - Get nb handled messages: ";
}
